(define-module (git notveryreproducible)
  #:use-module (ice-9 popen)
  #:use-module (ice-9 rdelim)
  #:use-module (gnu packages)
  #:use-module (gnu packages admin)
  #:use-module (guix build-system gnu)
  #:use-module (guix gexp)
  #:use-module (guix git-download)
  #:use-module (guix licenses)
  #:use-module (guix packages))

(define %source-dir (getcwd))

(let ((version "0")
      (revision "0")
      (commit (read-string (open-pipe "git show HEAD | head -1 | cut -d ' ' -f 2" OPEN_READ))))
  (package
    (name "notveryreproducible.git")
    (version (string-append version "-" revision "." (string-take commit 7)))
    (source (local-file %source-dir
                        #:recursive? #t
                        #:select? (git-predicate %source-dir)))
    (build-system gnu-build-system)
    (arguments
     (list
      #:parallel-build? #f ; makes output easier to read
      #:make-flags #~(list (string-append "DESTDIR=" #$output))
      #:phases #~(modify-phases %standard-phases
                   (add-after 'unpack 'avoid-installing-in-usr
                     (lambda _
                       (substitute* "Makefile"
                         (("/usr/share/notveryreproducible")
                          "/share/notveryreproducible"))))
                   (add-after 'unpack 'skip-some-nondeterminism
                     (lambda _
                       (substitute* "Makefile"
                         (("^domainname.*") ; domainname command not available?
                          ""))
                       (substitute* "Makefile"
                         (("^paths .*") ; checked paths not present
                          ""))
                       (substitute* "Makefile"
                         (("^localtime .*") ; checked file not present
                          ""))))
                   ;; No check target
                   (delete 'check)
                   ;; No configure script
                   (delete 'configure))))
    (native-inputs (list inetutils))
    (home-page
     "https://salsa.debian.org/reproducible-builds/notveryreproducible")
    (synopsis "Not Very Reproducible")
    (description "This package attempts to embed various aspects of the build
environment into the package itself.")
    (license gpl3+)))

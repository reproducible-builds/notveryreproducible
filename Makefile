#!/usr/bin/make -f
# Copyright 2023 Vagrant Cascadian <vagrant@reproducible-builds.org>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

targets = \
buildpath \
date \
domainname \
env \
hostname \
id \
localtime \
path \
paths \
umask \
uname \

INSTALL_PROGRAM = install
DESTDIR = unreproducible

%:
	# run command $@ and save output to file of same name
	$@ > $@
all: $(targets)
	: dependency target
buildpath:
	echo $(CURDIR) > buildpath
localtime: 
	ls -l /etc/localtime > localtime
path:
	echo $(PATH) > path
paths:
	ls -ld /bin > paths
	ls -ld /sbin >> paths
	ls -ld /lib >> paths
	ls -l /bin/sh >> paths
uname:
	uname -a > uname
test:
	: no tests
clean distclean realclean:
	rm -rvf $(targets)
install: $(targets)
	mkdir -p $(DESTDIR)/usr/share/notveryreproducible/
	for t in $(targets) ; do $(INSTALL_PROGRAM) $$t $(DESTDIR)/usr/share/notveryreproducible/ ; done

project = notveryreproducible
version = $(shell git describe --tags --long --dirty)
tarball:
	git archive --format=tar.gz HEAD --prefix=$(project)-$(version)/ --output=../$(project)-$(version).tar.gz

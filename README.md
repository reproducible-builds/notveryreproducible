The "notveryreproducible" package attempts to quickly capture very
basic information about the build environment and embed it into the
produced package.

The "theunreproduciblepackage" is a somewhat similar project which
contains many examples of reproducibility issues in code form:

  https://github.com/bmwiedemann/theunreproduciblepackage

These sorts of example "misbehaving" packages can be useful to test
tooling designed to seek out reproducibility issues, such as
reprotest:

  https://salsa.debian.org/reproducible-builds/reprotest

It is also useful to test if build environments or systems that are
designed to normalize the environment are working correctly.

For more information about why reproducibility matters, please check
out the Reproducible Builds project:

  https://reproducible-builds.org


Copyright 2023 Vagrant Cascadian <vagrant@reproducible-builds.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
